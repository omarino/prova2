import { Asset } from "../models/asset";

export async function getAssets() {
    return fetch("https://rest-sandbox.coinapi.io/v1/assets", {
        headers: {
            "X-CoinAPI-Key": "7F9F5372-04E8-4317-A58E-59C01BD47D95",
        },
    })
    .then(res => res.json())
    .then(assets => assets.map(a => new Asset (a.asset_id, a.name)));
}