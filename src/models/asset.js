export class Asset {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}