import "core-js/stable";
import "regenerator-runtime/runtime";
// seguo la documentazione di chart.js 
// importo il costruttore Chart dalla cartella chart.js in node_modules
import Chart from "chart.js/auto";

// creo il contesto del grafico
// creo il grafico come da documentazione dopo aver creatogli lo spazio nell' HTML
const ctx = document.getElementById("chart").getContext("2d");

const data = [];
const labels = [];
const chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels,
        datasets: [{
            label: 'Esercizio',
            data,
            fill: false,
            borderColor: "rgb(75, 192, 192)",
            tension: 0.1
        }],
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

// creo il collegamento per ricevere i dati con il socket
const sck = new WebSocket("ws://localhost:9000");

// metto un ascoltatore con evento "message" per ascoltare il messaggio restituito dal backend
sck.addEventListener("message", ev => {
    // prendo i dati e una volta "convertiti" tramite JSON.parse li metto in una constante
    const dt = JSON.parse(ev.data);

    // metti i valori di dt(i valori sono da trasformare in numerici) nell'array vuoto "data"
    data.push(+dt.value);
    
    // non sono sicuro che questa parte vada qui o nell' setInterval (dipende dal backend di andrea)
    const y = JSON.parse(ev.time);

    labels.push(y.value);
});



// setto l'azione che deve ripertersi quindi sicuramente il chart.update()
setInterval(() => {
    // non so se scrivere così ha senso perchè in teoria io metterei (data.push(+dt.value)) ma l'ho già scritto nell'ascoltatore
    data,
    // metterei qui la parte del .push (non sono sicuro)
    labels,
    chart.update();
}, 1000)