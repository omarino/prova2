const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 9000});

wss.on('connection', (ws, req) => {
    setInterval(() => {
        ws.send(JSON.stringify({
            value:(Math.random() * 100).toFixed(2),
            time: new Date().toLocaleString()
        }));
    }, 1000)
});