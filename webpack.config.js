module.exports = {
    devtool: 'source-map',
    entry: {
        coin: './src/coin.js',
        main: './src/index.js',
        table: './src/tabella.js',
        lazio: './src/lazio.js',
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}